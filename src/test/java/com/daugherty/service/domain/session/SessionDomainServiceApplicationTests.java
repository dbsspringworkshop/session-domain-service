package com.daugherty.service.domain.session;

import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Base64;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SessionDomainServiceApplicationTests {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private SessionRepository repository;

    @Test
    public void contextLoads() {
        assertThat(repository, is(notNullValue()));
    }

    @Test
    public void getCourses() throws Exception {
        Long initialCount = repository.count();

        MockHttpServletRequestBuilder request = get("/sessions");

        setStandardHeaders(request);

        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json;charset=UTF-8"))
                .andExpect(jsonPath("$._embedded.sessions", hasSize(initialCount.intValue())))
                .andExpect(jsonPath("$._links.self.href", endsWith("/sessions")))
                .andExpect(jsonPath("$.page.size", is(20)));
    }

    @Test
    public void getCoursesByStudent() throws Exception {
        Long initialCount = repository.count();

        MockHttpServletRequestBuilder request = get("/sessions/search/byStudent?student=1");

        setStandardHeaders(request);

        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json;charset=UTF-8"))
                .andExpect(jsonPath("$._embedded.sessions", hasSize(initialCount.intValue())))
                .andExpect(jsonPath("$._links.self.href", endsWith("/search/byStudent?student=1")))
                .andExpect(jsonPath("$.page.size", is(20)));
    }

    @Test
    public void getCoursesByCourse() throws Exception {
        Long initialCount = repository.count();

        MockHttpServletRequestBuilder request = get("/sessions/search/byCourse?course=1");

        setStandardHeaders(request);

        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json;charset=UTF-8"))
                .andExpect(jsonPath("$._embedded.sessions", hasSize(initialCount.intValue())))
                .andExpect(jsonPath("$._links.self.href", endsWith("/search/byCourse?course=1")))
                .andExpect(jsonPath("$.page.size", is(20)));
    }

    private static void setStandardHeaders(MockHttpServletRequestBuilder request) {
        HttpHeaders httpHeaders = new HttpHeaders();
        String auth = "user:springworkshop";
        String base64auth = Base64.getEncoder().encodeToString(auth.getBytes());
        String authHeader = "Basic " + base64auth;

        httpHeaders.set("Authorization", authHeader);
        request.headers(httpHeaders);
    }

}
