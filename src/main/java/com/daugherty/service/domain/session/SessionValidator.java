package com.daugherty.service.domain.session;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * @author garethdavies
 *
 */
@Component
public class SessionValidator implements Validator {

  @Override
  public boolean supports(Class<?> clazz) {
    return Session.class.equals(clazz);
  }

  @Override
  public void validate(Object target, Errors errors) {
    Session session = (Session) target;
    ValidationUtils.rejectIfEmpty(errors, "start", "start.empty", "Start time is required");
    ValidationUtils.rejectIfEmpty(errors, "end", "end.empty", "End time is required");
    if (session.getSignedIn() != null &&
        (session.getSignedIn().before(session.getStart()) || session.getSignedIn().after(session.getEnd()))) {
      errors.rejectValue("signedIn", "signedIn.outOfRange", "Sign in must be between Start and End");
    }
  }

}
