package com.daugherty.service.domain.session;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;

@SpringBootApplication
@EnableDiscoveryClient
public class SessionDomainServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(SessionDomainServiceApplication.class, args);
  }

  @Bean
  public RepositoryRestConfigurer repositoryRestConfigurer() {
    return new RepositoryRestConfigurerAdapter() {
      @Override
      public void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {
        validatingListener.addValidator("beforeCreate", new SessionValidator());
        validatingListener.addValidator("beforeSave", new SessionValidator());
      }
    };
  }

  @Bean
  public ResourceProcessor<Resource<Session>> sessionProcessor() {
    return new ResourceProcessor<Resource<Session>>() {

      @Override
      public Resource<Session> process(Resource<Session> resource) {
        Session session = resource.getContent();
        resource.add(new Link("http://localhost:10001/courses/" + session.getCourse(), "course"));
        resource.add(new Link("http://localhost:10002/students/" + session.getStudent(), "student"));
        return resource;
      }
    };
  }
}
