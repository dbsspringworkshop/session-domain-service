package com.daugherty.service.domain.session;

import java.util.Date;

import org.springframework.data.annotation.Id;

/**
 * @author garethdavies
 *
 */
public class Session {

  @Id
  private String id;

  private String course;
  private String student;

  private Date start;
  private Date end;
  private Date signedIn;

  public String getCourse() {
    return course;
  }

  public void setCourse(String course) {
    this.course = course;
  }

  public String getStudent() {
    return student;
  }

  public void setStudent(String student) {
    this.student = student;
  }

  public Date getStart() {
    return start;
  }

  public void setStart(Date start) {
    this.start = start;
  }

  public Date getEnd() {
    return end;
  }

  public void setEnd(Date end) {
    this.end = end;
  }

  public Date getSignedIn() {
    return signedIn;
  }

  public void setSignedIn(Date signedIn) {
    this.signedIn = signedIn;
  }

}
