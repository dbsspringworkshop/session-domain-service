package com.daugherty.service.domain.session;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * @author garethdavies
 *
 */
public interface SessionRepository extends MongoRepository<Session, String> {

  @RestResource(path = "byCourse", rel = "course")
  Page<Session> findByCourse(@Param("course") String course, Pageable pageable);

  @RestResource(path = "byStudent", rel = "student")
  Page<Session> findByStudent(@Param("student") String student, Pageable pageable);

}
